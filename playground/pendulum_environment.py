from tensorforce.environments import Environment
import numpy as np


class PendulumEnvironment(Environment):
    """
    https://tensorforce.readthedocs.io/en/latest/environments/environment.html?highlight=Environment
    """

    def __init__(self, m, g, l, dt, input_torque):
        super().__init__()

        # parameters
        self.m = m  # mass
        self.g = g  # gravity acceleration
        self.l = l  # half lenght of pole
        self.inertia = m*l*l/3.0  # moment of inertia
        self.input_torque = input_torque  # maximum input torque
        self.dt = dt  # simulation time step

    def states(self):
        # return shape of state
        # theta(rad) and d_theta(rad/s)
        return dict(type='float', shape=(2,))

    def actions(self):
        # return shape of action
        # input torque(scalar, N m)
        return dict(
            type='float',
            min_value=-self.input_torque,
            max_value=self.input_torque)

    def max_episode_timesteps(self):
        return super().max_episode_timesteps()

    def close(self):
        super().close()

    def reset(self):
        # reset state
        # set theta=0, d_theta = 0
        self.state = np.zeros(2)
        return self.state

    def execute(self, actions):
        # simulate pendulum dynamics and update self.state
        dt, m, g, l = self.dt, self.m, self.g, self.l
        theta, d_theta = self.state[0], self.state[1]

        self.state += dt * np.array([
            d_theta,
            (-m*g*l*np.sin(theta) + actions)/self.inertia
        ])

        # terminal state(whether pendulum is inverted or not)
        terminal = np.cos(self.state[0]) < -0.9
        # rewared: the height of the tip
        reward = -2*l*np.cos(self.state[0])
        return self.state, terminal, reward
