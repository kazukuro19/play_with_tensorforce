from tensorforce.agents import Agent
import numpy as np
from playground.pendulum_environment import PendulumEnvironment
from playground.qtable_agent import QtableAgent
from copy import deepcopy


def main():
    import matplotlib.pyplot as plt

    dt = 0.05
    m = 1.0
    g = 9.8
    l = 1.0
    environment = PendulumEnvironment(m, g, l, dt, 0.1*m*g*l)

    if input("use qtalbe?(y/n)").strip() == "n":
        agent = Agent.create(
            agent='tensorforce', environment=environment, update=64,
            optimizer=dict(optimizer='adam', learning_rate=1e-3),
            objective='policy_gradient', reward_estimation=dict(horizon=20)
        )
    else:
        agent = QtableAgent(
            action_candidates=np.linspace(-0.1*m*g*l, 0.1*m*g*l, 7),
            quantization=[
                (-3.1415, +3.1415, 11),
                (-10.0, +10.0, 21)],
            epsilon=0.1,
            alpha=0.5,
            gamma=0.9,
        )

    # learning
    n_epoch = 100
    for i in range(n_epoch):
        states_list = []
        actions_list = []
        terminal_list = []
        reward_list = []

        states = environment.reset()
        terminal = False
        print("%d th learning..." % i)
        while not terminal:
            actions = agent.act(states=states)
            states, terminal, reward = environment.execute(actions=actions)
            agent.observe(terminal=terminal, reward=reward)

            states_list.append(deepcopy(states))
            actions_list.append(actions)
            terminal_list.append(terminal)
            reward_list.append(reward)

    # display
    states_list = np.array(states_list)
    thetas = states_list[:, 0]
    d_thetas = states_list[:, 1]
    ts = np.linspace(0, dt*len(thetas), num=len(thetas))
    plt.subplot(3, 1, 1)
    plt.plot(ts, thetas)
    plt.ylabel('theta')
    plt.subplot(3, 1, 2)
    plt.plot(ts, d_thetas)
    plt.ylabel('d_theta')
    plt.subplot(3, 1, 3)
    plt.plot(ts, actions_list)
    plt.ylabel('action')
    plt.show()


if __name__ == "__main__":
    main()
