# play_with_tensorforce

## libraries
Verified libraries verisons in Python3.8.8.

```text
numpy==1.19.5
Tensorforce==0.6.5
tensorflow==2.6.0
keras==2.6.0
matplotlib==3.4.3
```